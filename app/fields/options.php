<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

acf_add_options_page([
    'page_title' => get_bloginfo('name') . ' theme options',
    'menu_title' => 'Opciones Home',
    'menu_slug'  => 'theme-options',
    'capability' => 'edit_theme_options',
    'position'   => '999',
    'autoload'   => true
]);

$options = new FieldsBuilder('theme_options');

$options
    ->setLocation('options_page', '==', 'theme-options');

$options
    ->addTab('Header', ['placement' => 'top'])
        ->addImage('logo-header', [
            'label' => 'Imagen para el logo',
        ])
    ->addImage('imagenParaFondo', [
        'label' => 'Imagen para el fondo fdel encabezado',
    ])
    ->addTab('Destacados Home', ['placement' => 'top'])
    ->addColorPicker('colorSVG', [
        'label' => 'Selecciona el color de los iconos',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
    ])
    ->addNumber('tamanoIcono', [
        'label' => 'Ancho del icono',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],

        'prepend' => 'ancho icono',
        'append' => '%',
        'min' => '5',
        'max' => '100',
        'step' => '5',
    ])
    ->addNumber('altoIcono', [
        'label' => 'Alto del icono',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],

        'prepend' => 'alto icono',
        'append' => 'px',
        'min' => '30',
        'max' => '200',
        'step' => '10',
    ])
    ->addRepeater('agregarDestacados', [
                'label' => 'Destados Para Reuniones',
                'layout' => 'row',
                'button_label' => 'Agregar Destacado',
            ])

    ->addText('codigoSvg', [
        'label' => 'Introduce el codigo svg para agregar un icono',
        'instructions' => '',
    ])
    ->addText('viewBoxSvg', [
        'label' => 'Introduce el viewBox poscision del svg para el icono',
        'instructions' => '',
    ])
        ->addText('titDestacado', [
            'label' => 'Titular para el destacado del encabezado',
            'instructions' => '',
        ])
        ->addTextarea('txtDestacado', [
            'label' => 'Texto para el destcado',
        ])
    ->endRepeater()


    ->addTab('Aliados', ['placement' => 'top'])
        ->addText('tit_seccion_aliados', [
            'label' => 'Titulo para la seccion de colaboradores',
            'instructions' => '',
        ])
        ->addRepeater('listado_aliados', [
                'label' => 'Texto para listado de aliados',
                'instructions' => '',
                'layout' => 'block',

                ])
                ->addText('mombre_aliados', [
                        'label' => 'Nombre para el listado de los aliados',
                        'instructions' => '',
                    ])
                ->addImage('logo_aliados', [
                    'label' => 'Logo para los aliados',
                    'instructions' => '',
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                ])
            ->endRepeater()




   ;
return $options;

