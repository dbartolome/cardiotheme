<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$comunicacion = new FieldsBuilder('campos_comunicacion');

$comunicacion
    ->setLocation('post_type', '==', 'comunicacion');

$comunicacion


    ->addText('nombre', [
        'label' => 'Nombre',
        'instructions' => 'Nombre del autor del comunicado',
    ])
    ->addTextarea('autores', [
        'label' => 'Autores de la comunicacion',
        'instructions' => '',
        'required' => 0,
    ])
    ->addTextarea('centro-trabajo', [
        'label' => 'centro de trabajo de los autores',
        'instructions' => '',
        'required' => 0,
    ])
    ->addFile('comunicado-pdf', [
        'label' => 'Poster de la comunicacion',
        'instructions' => '',
        'mime_types' => 'pdf',
    ])
    ->addImage('portada-comunicacion', [
        'label' => 'imagen para poster del comunicado',
        'instructions' => '',

    ]);

return $comunicacion;
