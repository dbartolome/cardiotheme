<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$sesion = new FieldsBuilder('campos_sesion');

$sesion
    ->setLocation('post_type', '==', 'sesion');

$sesion
    ->addText('urlVideo', [
        'label' => 'Url del video',
        'instructions' => 'Introducir la url del video',
    ]);

return $sesion;

