<?php
namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$sala = new FieldsBuilder('campos_sala');

$sala
    ->setLocation('post_type', '==', 'sala');

$sala
    ->addText('idSala', [
        'label' => 'Id de la sala de reunion',
        'instructions' => '',
    ])
    ->addTrueFalse('salaChat', [
        'label' => 'Selecciona si tien sala de chas',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])

;

return $sala;
