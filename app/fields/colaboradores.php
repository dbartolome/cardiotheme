<?php
namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$colaborador = new FieldsBuilder('campos_colobador');

$colaborador
    ->setLocation('post_type', '==', 'colaborador');

$colaborador
    ->addImage('image_stand', [
        'label' => 'Imagen del stand para mostrar en el repo',
        'instructions' => '',
    ])
    ->addImage('logo-laboratorio', [
        'label' => 'Imagen del logo del laboratorio',
        'instructions' => '',
    ])
    ->addRepeater('slider', [
        'label' => 'Slides y links para colaboradores',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'min' => 0,
        'max' => 15,
        'layout' => 'table',
    ])
    ->addImage('imagen_slide', [
        'label' => 'Imagen para el slide',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '30',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addUrl('url_slide', [
        'label' => 'URL para el slider',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
    ])

;

return $colaborador;
