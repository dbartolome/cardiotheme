<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$acceso_page = get_page_by_path('acceso', 'ARRAY_N');

$acceso = new FieldsBuilder('acesso');

$acceso
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-acceso.blade.php');

$acceso
    ->addText('text_boton', [
        'label' => 'Texto del botnn de acceso a la reunion',
        'instructions' => '',
    ])
    ->addPageLink('linkBottom', [
        'label' => 'link para la pagina de la reunion',
        'type' => 'page_link',
        'post_type' => ['page'],
        'taxonomy' => [],

    ]);

return $acceso;
