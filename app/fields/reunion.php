<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;



$reunion = new FieldsBuilder('reuniones');

$reunion
    ->setLocation('post_type', '==', 'reunion');


$reunion
    ->addTab('Fecha de la reunion', ['placement' => 'left'])
        ->addDatePicker('fechaReunion', [
            'label' => 'Dia en el qeu se celebrara la reunion',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'display_format' => 'd-m-Y',
            'return_format' => 'd-m-Y',
            'first_day' => 1,
        ])
    ->addTab('Header', ['placement' => 'left'])
        ->addText('fecha-destacadoreuniones', [
            'label' => 'Fecha para el destacado del encabezado',
            'instructions' => '',
            ])
        ->addTextarea('entrada-destacadoreuniones', [
            'label' => 'Entrada para el destacado del encabezado',
            'instructions' => '',
            ])
    ->addTab('Inscripcion', ['placement' => 'left'])
        ->addText('textobotonInscripcion', [
            'label' => 'Texto para el boton de inscripciones',
            'instructions' => '',
            ])
    ->addTab('Programa', ['placement' => 'top'])
        ->addText('tit_seccion_programareuniones', [
            'label' => 'Titulo para la seccion de programa',
            'instructions' => '',
        ])
        ->addFile('subir_programareuniones', [
            'label' => 'Subir el programa en pdf',
            'return_format' => 'array',
            'library' => 'all',
            'mime_types' => 'pdf',
        ])
        ->addText('texto_boton_descargareuniones', [
            'label' => 'Texto para el boton de visualizacion del programa',
            'instructions' => '',
        ])
    ->addTab('Acceso Reunion', ['placement' => 'top'])
        ->addPageLink('accesoReunion', [
            'label' => 'Enlace para el boton de inscripciones',
            'type' => 'page_link',
            'post_type' => ['sala'],
            'allow_null' => 1,
        ])
    ->addUrl('urlAcceso', [
        'label' => 'URL Acceso a la reunion cuando se hace fuera de ecastellanacardio',
    ])
        ->addText('textobotonAcceso', [
            'label' => 'Texto para el boton de Acceso reuniones',
            'instructions' => '',
        ])
    ->addTab('Videos Youtube', ['placement' => 'left'])
        ->addUrl('urlVideo', [
            'label' => 'URL del video en youtube o canal de youtube',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
        ])
        ->addText('textobotonYoutube', [
            'label' => 'Texto para el boton de Acceso reuniones',
            'instructions' => '',
        ])

    ->addTab('Patrocinadores', ['placement' => 'left'])
        ->addText('titPatrocinadores', [
            'label' => 'Titulo para la seccion de patrocinadores',
            'instructions' => '',
        ])
        ->addRepeater('imagenes_colaboradoresreuniones', [
            'label' => 'Imagenes para los colaboradores de la home',
            'instructions' => '',
            'layout' => 'block',
         ])
            ->addImage('logo_colaboradoresreuniones', [
                'label' => 'Logo para colaboradores',
                'instructions' => '',
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
            ])
        ->endRepeater()
    ->addTab('Colaboradores', ['placement' => 'left'])
        ->addPageLink('paginaColaboradores', [
            'label' => 'Enlace para el boton de inscripciones',
            'type' => 'page_link',
            'post_type' => ['page'],
            'allow_null' => 1,
        ])
        ->addText('txtBtnColaboradores', [
            'label' => 'Texto para el boton de la pagina colaboradores  ',
            'instructions' => '',
        ])
    ->addTab('Comunicaciones', ['placement' => 'left'])
        ->addPageLink('paginaComunicaiones', [
            'label' => 'Enlace para el boton de comunicaciones',
            'type' => 'page_link',
            'post_type' => ['page'],
            'allow_null' => 1,
        ])
        ->addText('txtBtnComunicaciones', [
            'label' => 'Texto para el boton de la pagina Comunicaciones  ',
            'instructions' => '',
        ])
    ->addTab('Formulario de inscripcion', ['placement' => 'left'])
        ->addText('titSeccionFormulario', [
            'label' => 'Titulo de la seccion formulario de inscripción',
            'instructions' => '',
        ])
        ->addText('titFormulario', [
            'label' => 'Titulo del formulario de inscripción',
            'instructions' => '',
        ])
;

return $reunion;

