<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$streaming_page = get_page_by_path('streaming', 'ARRAY_N');

$streaming = new FieldsBuilder('streaming');

$streaming
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-streaming.blade.php');

$streaming
    ->addTextarea('programa_reunion', [
        'label' => 'Textarea Field',
        'instructions' => 'Agregar el programa de ese dia',
        'required' => 0,
    ])
;

return $streaming;


