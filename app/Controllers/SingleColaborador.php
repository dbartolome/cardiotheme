<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleColaborador extends Controller
{

    public function sliderColaborador() {
        $output = '';
        $slideContent = '';
        $indicadores = '';
          // check if the repeater field has rows of data

          if( have_rows('slider') ):
            $i = 1;
             while ( have_rows('slider') ) : the_row();
              // display a sub field value
              $image = get_sub_field('imagen_slide');
              $urlSlide = get_sub_field('url_slide');
              if($i == 1) {
                  $indicadores .= '<li data-target="#carouselExampleIndicators" data-slide-to="' . $i . '" class="active"></li>';
                  $slideContent .= '<div class="carousel-item active"><a href="'. $urlSlide .'" target="_blank"><img src="' . $image['url'] . '" class="d-block w-100" alt="' . $image['alt'] . '"></a></div>';
                  } else {
                  $indicadores .= '<li data-target="#carouselExampleIndicators" data-slide-to="' . $i . '"></li>';
                  $slideContent .= '<div class="carousel-item">
                        <a href="'. $urlSlide .'" target="_blank"> <img src="' . $image['url'] . '" class="d-block w-100" alt="' . $image['alt'] . '"></a>
                     </div>';
              }
                $i++;
            endwhile; ?>
            <?php
          else :

            // no rows found

          endif;


           $output .= '<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">'. $indicadores .'

                </ol>
                <div class="carousel-inner">'. $slideContent .'

                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>';

        return $output;

    }

}
