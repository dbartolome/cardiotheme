<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class
FrontPage extends Controller
{
    /*
     * header para la home
     */
    public function logoHeader()
    {
        $output = '';
        $image = get_field('logo-header', 'option');
        if( $image ) {
            $output = $image['url'];
        }
        return $output;
    }
}
