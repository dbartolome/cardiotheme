{{--
  Template Name: Plantilla Para cartas de bienvenida
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10 col-lg-8">
        @include('partials.content-personalizado')
      </div>
    </div>
  </div>

  @endwhile
@endsection
