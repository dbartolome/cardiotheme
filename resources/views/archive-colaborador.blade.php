@extends('layouts.app')

@section('content')


@if (!have_posts())
<div class="alert alert-warning">
  {{ __('Sorry, no results were found.', 'sage') }}
</div>
{!! get_search_form(false) !!}
@endif

@php
  $args = array('post_type'=>'colaborador','posts_per_page'=>'-1');
  $loop = new WP_Query($args);
@endphp

<div class="container">
<div class="row">
@while ( $loop->have_posts()) @php $loop->the_post() @endphp

@include('partials.content-'.get_post_type())

@endwhile
</div>
</div>


@endsection

