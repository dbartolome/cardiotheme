@extends('layouts.app')

@section('content')
<?php
$imEncabezado = get_field('imagenParaFondo','options');
?>
  <section class="header" style="padding: 2% 0; background: url(<?php echo $imEncabezado["url"]; ?>); background-size: cover; background-position: bottom;">
    @include('partials.front-page.carruselheader')
  </section>

  <section class="destacados" id="destacadosHome">
  <div class="container">
  <div class="row">
  <?php
    $contnidoDestacado = '';


      if( have_rows('agregarDestacados', 'options') ):
        $colorIconos = get_field('colorSVG', 'options');
        $tamanoIcono= get_field('tamanoIcono', 'options');
        $altoIcono= get_field('altoIcono', 'options');
          while( have_rows('agregarDestacados','options') ) : the_row();
              $imgDestacado = get_sub_field('iconoDestacado');
              $titDestacado = get_sub_field('titDestacado');
              $textoDestacado = get_sub_field('txtDestacado');
              $svgDestacado = get_sub_field('codigoSvg');
              $codViewBox = get_sub_field('viewBoxSvg');
            $contnidoDestacado .='<div class="col-md-4 p-4 my-1">
                              <div style="text-align: center; margin-bottom: 3%;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="'.$codViewBox.'" style="min-height: '.$altoIcono.'; width: '.$tamanoIcono.'%;"><defs><style>.cls-1{fill:none;stroke:'.$colorIconos.';stroke-linecap:round;stroke-linejoin:round;stroke-width:3.5px;}.cls-2{fill:none;stroke:'.$colorIconos.';stroke-linecap:round;stroke-linejoin:round;}.cls-2{stroke-width:3.46px;}</style></style></defs>'. $svgDestacado .'</svg></div>
                              <h3 class="titDestacadoIco">'.$titDestacado.'</h3>
                              <p>'.$textoDestacado.'</p>
                            </div>';

          endwhile;



      // No value.
      else :
          // Do something...
      endif;

      echo $contnidoDestacado;
?>
  </div>
</div>
  </section>



    <section class="aliadosHome">
      <h2>{!! get_field('tit_seccion_aliados', 'options') !!}</h2>
      <div class="container">
        <div class="row justify-content-md-center">
          <!-- <div class="col-12" style="font-size: 1.3rem; text-transform: uppercase; font-weight: bold; text-align: center;">Abbott - Almirall - Amgen - AstraZeneca - Bayer - Biosensors - Biotronik - Boehringer Ingelheim Lilly - Boston Scientific - Bristol Myers Squibb Pfizer - Cardiva - Ferrer - Medtronic - Novartis - Rovi - Sanofi - Servier - Terumo - </div> -->

          <?php
          $contnidoDestacado = '';
          if( have_rows('listado_aliados', 'options') ):
            while( have_rows('listado_aliados','options') ) : the_row();
              $imgAliado = get_sub_field('logo_aliados');
              $momAliados = get_sub_field('mombre_aliados');


              $contnidoDestacado .='<div class="col-md-3 p-4">
                              <div style="text-align: center; margin-bottom: 3%;"><img src="'. $imgAliado['url'] .'" width="100%" style=" -webkit-filter: grayscale(100%);
    filter: grayscale(100%); max-width: 150px;"></div>


                            </div>';
            endwhile;
          // No value.
          else :
            // Do something...
          endif;

          echo $contnidoDestacado;
          ?>

        </div>
      </div>

    </section>





<div class="btnSubir"><img src="/wp-content/themes/cardiotheme/dist/images/subir.png" width="100%"> </div>
@endsection

