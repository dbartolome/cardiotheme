{{--
  Template Name: Tema para las reuniones
--}}

@extends('layouts.app')

@if (!have_posts())
  <div class="alert alert-warning">
    {{ __('Sorry, no results were found.', 'sage') }}
  </div>
  {!! get_search_form(false) !!}
@endif

@php
  $args = array('post_type'=>'reunion','posts_per_page'=>'-1');
  $loop = new WP_Query($args);
@endphp

@section('content')
  <div class="container">
    <h3 style="text-transform: uppercase">Próximas Reuniones</h3>
    <div class="row mb-5">
    @while ($loop->have_posts()) @php $loop->the_post() @endphp
      <?php
        $diaReunion = get_field('fechaReunion');
        $diaHoy = date("d-m-Y");
        $datetime1 = new DateTime($diaHoy);
        $datetime2 = new DateTime($diaReunion);
        $interval = $datetime1->diff($datetime2);
        $diferenciaDias = 0;
        $diferenciaDias = $interval->format('%R%a');


        if($diferenciaDias >=0) { ?>
          @include('partials.content-reunionlistado')
        <?php
        }
        ?>
      @endwhile
    </div>
    <h3 style="text-transform: uppercase">Reuniones anteriores</h3>
    <div class="row mb-5">

      @while ($loop->have_posts()) @php $loop->the_post() @endphp
      <?php
      $diaReunion = get_field('fechaReunion');
      $diaHoy = date("d-m-Y");
      $datetime1 = new DateTime($diaHoy);
      $datetime2 = new DateTime($diaReunion);
      $interval = $datetime1->diff($datetime2);
      $diferenciaDias = 0;
      $diferenciaDias = $interval->format('%R%a');


      if($diferenciaDias < 0) { ?>
      @include('partials.content-reunionlistado')
      <?php
      }
      ?>
      @endwhile
    </div>
  </div>

@endsection





