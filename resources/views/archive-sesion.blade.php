@extends('layouts.app')

@section('content')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  @php
    $args = array('post_type'=>'sesion','posts_per_page'=>'-1','orderby'=>'date', 'order' => 'ASC');
    $loop = new WP_Query($args);
  @endphp
  <div class="container">
    <div class="row mb-5" id="video-thumbnails">
      @while ($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-'.get_post_type())
      @endwhile
    </div>

@endsection
