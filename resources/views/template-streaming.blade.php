{{--
  Template Name: Plantilla Para el streaming
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container">
    <div class="row">
      <div class="col-12">
        @include('partials.content-personalizado')
      </div>
    </div>
  </div>
  <div class="container">
   <?php
   $contDia = get_field('programa_reunion');
   echo $contDia;
   ?>
  </div>

  @endwhile
@endsection

