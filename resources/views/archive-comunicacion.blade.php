@extends('layouts.app')

@section('content')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  @php
    $args = array('post_type'=>'comunicacion','posts_per_page'=>'-1');
    $loop = new WP_Query($args);
  @endphp
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="infoConcurso"><strong>Concurso de Comunicaciones </strong><br />Votación: Cada persona inscrita sólo puede dar 1 voto (estrella) a su Comunicación Oral favorita y 3 votos (estrella) a sus tres Pósters preferidos. <br />En caso de clicar más veces, se verá anulada toda su votación   </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 botonesFiltro">
        <button class="botnDesplegable activo" id="verOrales">Orales</button><button class="botnDesplegable" id="verPosters">Posters</button>
      </div>
    </div>
    <div class="row mt-5">
      @while ($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-'.get_post_type())
      @endwhile
    </div>

@endsection

