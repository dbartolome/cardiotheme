{{--
  Template Name: Plantilla Personalizada
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container">
    <div class="row">
      <div class="col-12">
  @include('partials.content-personalizado')
      </div>
    </div>
  </div>

  @endwhile
@endsection
