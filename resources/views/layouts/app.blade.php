<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body @php body_class() @endphp>
@include('partials.menu-header')
@php do_action('get_header') @endphp
@if(is_front_page())
  @include('partials.header-home')
  <div class="wrap content-fluid" role="document">
    <div class="content-fluid">
      <main class="main content-fluid">
        @yield('content')
      </main>
    <!-- @if (App\display_sidebar())
      <aside class="sidebar">
@include('partials.sidebar')
        </aside>
@endif -->
    </div>
  </div>
@else
  @include('partials.header')
  <div class="wrap content" role="document">
    <div class="content-fluid">
      <main class="main content-fluid">
        @yield('content')
      </main>
    <!-- @if (App\display_sidebar())
      <aside class="sidebar">
@include('partials.sidebar')
        </aside>
@endif -->
    </div>
  </div>
@endif

@php do_action('get_footer') @endphp
@include('partials.footer')
@php wp_footer() @endphp
</body>
</html>
