{{--
  Template Name: Plantilla Para acceso
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10 col-lg-8">
        @php dynamic_sidebar('sidebar-primary') @endphp

        <p class="recordarPw"> (*) En caso de no recordar tu contraseña o tener problemas de acceso, ponte en contacto con info@ecastellanacardio.es</p>
      </div>
    </div>
    <div class="row justify-content-md-center btnAcceso">
      <?php
         $linkBoton =get_field('linkBottom');
        $textBoton =  get_field('text_boton');
      ?>
      <div class="col-6"><a href="<?php echo $linkBoton; ?>" class="btnNavegacion"><?php echo $textBoton; ?></a></div>
    </div>
  </div>

  @endwhile
@endsection
