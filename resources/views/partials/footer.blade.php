<footer class="content-info">
  <div class="container">
    <div class="row">
      <div class="col-12">
    @php dynamic_sidebar('sidebar-footer') @endphp
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        @php dynamic_sidebar('footer-bottom') @endphp
      </div>
    </div>
  </div>
</footer>
