<?php

  $diaReunion = get_field('fechaReunion');
  $diaHoy = date("d-m-Y");

  $datetime1 = new DateTime($diaHoy);
  $datetime2 = new DateTime($diaReunion);
  $interval = $datetime1->diff($datetime2);
  $diferenciaDias = 0;
  $diferenciaDias = $interval->format('%R%a');
?>

<?php
if($diferenciaDias > 0) {
  echo '<div class="capaDias">Faltan '.$interval->d.' día(s) para la reunión</div>';
} else if($diferenciaDias < 0){

} else {
  echo '<div class="capaDias">Hoy es el dia de la reunión</div>';
}
?>

<div class="container-fluid contReunionesGrid">
  <div class="row contReunionSlide filaReunion align-items-center" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
    <div class="col-12 align-self-center">

      <h1 class="entry-title">{!! get_the_title() !!}</h1>
      <p class="fechaReunion">{!! get_field('fecha-destacadoreuniones') !!}</p>
    </div>
  </div>
  <div class="row justify-content-center py-3 px-4">
    <div class="col-12">
    <p class="textoEncabezado"><?php echo $textoEncabezado; ?></p>
    </div>
  </div>
  <div class="row justify-content-center py-3 px-4">

    <?php
    $contBotones = '';
    $linkReunion = get_the_permalink();
    $txtBtnReunion = get_field('textobotonInscripcion');
    $linkDescarga = get_field('subir_programareuniones');
    $txtBtnDescarga = get_field('texto_boton_descargareuniones');

    $urlacceso = get_field('urlAcceso');
    $accesoReunion= get_field('accesoReunion');
    $txtBtnAccesoReunion= get_field('textobotonAcceso');
    $linkVideo = get_field('urlVideo');
    $txtBtnVideo = get_field('textobotonYoutube');
    $linkColaboradores = get_field('paginaColaboradores');
    $txtBtnColaboradores = get_field('txtBtnColaboradores');
    $linkComunicaciones = get_field('paginaComunicaiones');
    $txtBtnComunicaciones = get_field('txtBtnComunicaciones');

    $contadorBotones = 0;
    $lisatdoBotones = array();
    $contColumnas = 0;

    if($txtBtnReunion != ''){
      $lisatdoBotones[$contadorBotones]  = '<a href="'. $linkReunion .'#contenedorFormulario" class="btnNavegacion">'.$txtBtnReunion.'</a>';
      $contadorBotones++;
    }
    if($linkDescarga != '') {
      $lisatdoBotones[$contadorBotones]  = '<a href="'.$linkDescarga['url'].'" target="_blank" class="btnNavegacion">'. $txtBtnDescarga .'</a>';
      $contadorBotones++;
    }
    if($accesoReunion != ''){
      $lisatdoBotones[$contadorBotones]  = ' <a href="'.$accesoReunion.'" target="_blank" class="btnNavegacion">'.$txtBtnAccesoReunion.'</a>';
      $contadorBotones++;
    }

    if($urlacceso != ''){
      $lisatdoBotones[$contadorBotones]  = ' <a href="'.$urlacceso.'" target="_blank" class="btnNavegacion">'.$txtBtnAccesoReunion.'</a>';
      $contadorBotones++;
    }


    if($linkVideo != ''){
      $lisatdoBotones[$contadorBotones]  = '<a href="'.$linkVideo.'" target="_blank" class="btnNavegacion">'.$txtBtnVideo.'</a>';
      $contadorBotones++;
    }

    if($linkColaboradores != ''){
      $lisatdoBotones[$contadorBotones]  = '<a href="'.$linkColaboradores.'" target="_blank" class="btnNavegacion">'.$txtBtnColaboradores.'</a>';
      $contadorBotones++;
    }

    if($linkComunicaciones != ''){
      $lisatdoBotones[$contadorBotones]  = ' <a href="'.$linkComunicaciones.'" target="_blank" class="btnNavegacion">'.$txtBtnComunicaciones.'</a>';
      $contadorBotones++;
    }


    if( $contadorBotones <= 1) {
      $contColumnas = 12;
    } else  if( $contadorBotones == 2) {
      $contColumnas = 6;
    } else {
      $contColumnas = 4;
    }

    for ( $i= 0; $i<$contadorBotones; $i++ ) {
      $contBotones .= '<div class="col-12 col-md-'.$contColumnas.'">';
      $contBotones .= $lisatdoBotones[$i];
      $contBotones .= '</div>';
    }


    echo $contBotones;
    ?>
  </div>
  <div class="row filaReunion align-items-center py-3 px-4">
    <div class="col-12">
        <h2 class="titPatrocinadores">{!! get_field('titPatrocinadores') !!}</h2>
          <div class="row justify-content-md-center">

            <?php
            $listadoPatrocinadores = array();

               if( have_rows('imagenes_colaboradoresreuniones') ):
                 $i = 0;
               while( have_rows('imagenes_colaboradoresreuniones') ) : the_row();
                $imgPatrocinador = get_sub_field('logo_colaboradoresreuniones');
                $listadoPatrocinadores[$i] ='<div class="col-6 col-lg-3 p-4">
                                                <img src="'. $imgPatrocinador['url'] .'" width="100%">
                                            </div>';

              $i++;

          endwhile;
                 //if ($i == 3){
                   //break;
                // }



      // No value.
      else :
          // Do something...
      endif;
     // echo $listadoPatrocinadores;
            $varNumero = 0;
            if($i > 3) {
              $varNumero = 4;
            } else {
              $varNumero = $i;
            }
            if($varNumero > 1) {
              $patrocinadores_aleatorias = array_rand($listadoPatrocinadores, $varNumero);
              for($j = 0; $j<$varNumero; $j++) {
                echo $listadoPatrocinadores[$patrocinadores_aleatorias[$j]];
              }
            } else {
              echo $listadoPatrocinadores[0];
            }



      ?>
          </div>
    </div>
  </div>
 <!-- <div class="row justify-content-end align-items-center py-5 px-5">
  <?php

  //if($diferenciaDias >= 0) {
    //echo '
      //      <div class="col-12 col-md-6 col-lg-4" style="padding: 0;"><a href="'. get_the_permalink() .'" class="btnNavegacion" style="width:100%;">+ información</a></div>
       // ';
 // }
  ?>
  </div> -->

</div>
