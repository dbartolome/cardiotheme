
<div class="headerMenu menuTop">
  <div class="container">
    <div class="row align-items-center">
    <div class="col-3"  style="padding-left: 0; padding-right: 0">
      <a class="brand" href="{{ home_url('/') }}">
      <?php
      $img = get_field('logo-header', 'option');
      ?>
      <img src="<?php echo $img['url'] ?>">
    </a>
    </div>
    <div class="col-7 col-md-9" style="padding-left: 0; padding-right: 0">
    <nav class="navbar navbar-expand-md" role="navigation">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu($primarymenu) !!}
      @endif
    </nav>
    </div>
    <div class="col-2 col-md-1" style="padding: 0">

      <button class="hamburger hamburger--spin" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
      </button>

    </div>

    </div>
  </div>
</div>
</div>
<div class="colNavegacion">

    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu($primarymenu) !!}
    @endif



</div>
