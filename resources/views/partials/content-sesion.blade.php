<div class="col-12 col-md-6 col-lg-4 mt-3">
  <div style="position: relative">
    <?php
      $urlVideo = get_field('urlVideo');
    ?>
<a href="https://www.youtube.com/watch?v=<?php echo $urlVideo; ?>" data-sub-html="<h4><?php the_title(); ?></h4>">
  <img class="img-responsive" src="<?php the_post_thumbnail_url(); ?>" width="100%">
  <div class="demo-gallery-poster">
    <img src="/wp-content/themes/cardiotheme/dist/images/play-button.png">
  </div>
</a>
  </div>
    <h5><?php the_title(); ?></h5>

</div>

