<?php
$imagenStand = get_field('image_stand');
  ?>
<div class="col-12 col-md-6 col-lg-4 p-2">
  <a href="{!! the_permalink() !!}">
<article @php post_class() @endphp>
  <img src="{!! $imagenStand['url']; !!}" width="100%">
  <div class="tituloColaborador">
    {!! get_the_title() !!}
  </div>

</article>
  </a>
</div>
