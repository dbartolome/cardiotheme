<?php
$textoEncabezado = get_field('entrada-destacadoreuniones');

?>

  <div class="container">
    <div class="row">
      <div class="col-12">

        <p style="font-size: 1.4rem; text-align: center; color: #970e53; text-transform: uppercase;">{!! get_field('fecha-destacadoreuniones') !!}</p>
        <p style="font-size: 1rem; text-align: center; color: #970e53; font-weight: bold; text-transform: uppercase;"><?php echo $textoEncabezado; ?></p>
      </div>
    </div>
  </div>
<section id="contenedorPatrocinadores" style="background-color: #f1f1f1; padding: 3% 0;">
  <div class="container">
      <div class="row justify-content-md-center">
        <?php
        $contBotones = '';
        $linkReunion = get_field('paginaInscripcionmes');
        $txtBtnReunion = get_field('textobotonInscripcion');
        $linkDescarga = get_field('subir_programareuniones');
        $txtBtnDescarga = get_field('texto_boton_descargareuniones');
        $accesoReunion= get_field('accesoReunion');
        $txtBtnAccesoReunion= get_field('textobotonAcceso');
        $linkVideo = get_field('urlVideo');
        $txtBtnVideo = get_field('textobotonYoutube');
        if($linkReunion != ''){
          $contBotones .= '<div class="col-12 col-md-4">
                          <a href="'. $linkReunion .'" target="_blank" class="btnNavegacion" style="padding: 1% 0; text-align: center; display: block; width: 90%;  margin: 1% auto">'.$txtBtnReunion.'</a>
                          </div>';
        }
        if($linkDescarga != '') {
          $contBotones .= '<div class="col-12  col-md-4">
        <a href="'.$linkDescarga['url'].'" target="_blank" class="btnNavegacion" style="padding: 1% 0; text-align: center; display: block; width: 90%; margin: 1% auto">'. $txtBtnDescarga .'</a>
      </div>';
        }
        if($accesoReunion != ''){
          $contBotones .= ' <div class="col-12  col-md-4">
        <a href="'.$accesoReunion.'" target="_blank" class="btnNavegacion" style="padding: 1% 0; text-align: center; display: block; width: 90%; margin: 1% auto">'.$txtBtnAccesoReunion.'</a>
      </div>';
        }
        if($linkVideo != ''){
          $contBotones .= ' <div class="col-12  col-md-4">
        <a href="'.$linkVideo.'" target="_blank" class="btnNavegacion" style="padding: 1% 0; text-align: center; display: block; width: 90%; margin: 1% auto">'.$txtBtnVideo.'</a>
      </div>';
        }
        echo $contBotones;
        ?>
      </div>
  </div>
</section>
<section id="contenedorPatrocinadores" style=" padding: 3% 0;">
  <div class="container">
      <?php
      $listadoPatrocinadores = '';
      $contenedorPatrocinadores = '';
      $tituloSeccionPatrocinadores = get_field('titPatrocinadores');
      if( have_rows('imagenes_colaboradoresreuniones') ):
        while( have_rows('imagenes_colaboradoresreuniones') ) : the_row();
          $imgPatrocinador = get_sub_field('logo_colaboradoresreuniones');
          $listadoPatrocinadores .='<div class="col-6 col-lg-3 text-center">
                                <img src="'. $imgPatrocinador['url'] .'" width="60%">

                              </div>';
        endwhile;
      // No value.
      else :
        // Do something...
      endif;

      if($listadoPatrocinadores != '') {

      $contenedorPatrocinadores .= '<div class="row"><div class="col-12"><section class="patrocinadoresHome"><h2 style="font-size: 1.6rem; text-align: center; text-transform: uppercase; font-weight: bold; margin: 2% 0;">'.$tituloSeccionPatrocinadores.'</h2>
            <div class="container">
              <div class="row">'. $listadoPatrocinadores.'</div></div></section></div></div>';
            }

      echo $contenedorPatrocinadores;

  ?>
  </div>
</section>

    <?php
    $contFormulario = '';$contFormulario2 = '';
    $titFormulario = get_field('titSeccionFormulario');
    $formCodigo  = get_field('titFormulario');

    if($titFormulario != ''){
      $contFormulario .= '<section id="contenedorFormulario"><div class="container"><div class="row"><div class="col-12"><h2 style="text-align: center">'.$titFormulario.'</h2> ';
      $contFormulario .= do_shortcode( '[gravityform id="'.$formCodigo.'" title="false" description="false" ajax="true" tabindex="1"]');
      $contFormulario .= '</div></div></div></section>';
      echo $contFormulario;
    }



    echo $contFormulario2;
    ?>



