<?php
$imgFondoGenrica = get_field('img-fondo', 'option');
$imgFondoHeader = get_the_post_thumbnail_url();
if($imgFondoHeader == "") {
  //$imgFondoHeaderFinal = $imgFondoGenrica['url'];
} else {
  $imgFondoHeaderFinal = $imgFondoHeader;
}
?>
<header class="banner" style="background-image: url(<?php echo $imgFondoHeaderFinal; ?>);">
<div class="container">
  <div class="row align-items-center">
    <div class="col-12">
      <h1> <?php wp_title(''); ?></h1>
    </div>

  </div>
</div>
</header>
