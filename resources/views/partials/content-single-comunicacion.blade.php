<article @php post_class() @endphp>
<div class="container">
  <div class="row align-items-center">
    <div class="col-12 col-sm-4">
<?php  $imgDestacado = get_field('portada-comunicacion') ?>
      <img src="{!! $imgDestacado['url'] !!}" width="100%">
    </div>
    <div class="col-12 col-sm-8">
      <?php $nombreautor = get_field('nombre') ?>
      <div class="nombrAutor">{!! $nombreautor !!}</div>
        <h2>{!! the_title() !!}</h2>
       
      <div class="entry-content">
        <?php
        $autores = get_field('autores');
        if( $autores ): ?>
          <strong>Autores:</strong>
          <p><?php echo $autores ?></p>
        <?php endif;

        $centro = get_field('centro-trabajo');
        if($centro ): ?>
          <strong>Centro de trabajo:</strong>
          <p><?php echo $centro ?></p>
        <?php endif; ?>
      </div>
        <?php
        $filePortada = get_field('comunicado-pdf');
        if( $filePortada ): ?>
        <a href="<?php echo $filePortada['url']; ?>" target="_blank" class="btnDescarga">Descargar Poster</a>
        <?php endif; ?>

    </div>
  </div>

</div>

</article>
