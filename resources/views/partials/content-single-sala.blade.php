
<?php
 $tieneSala = get_field('salaChat');
 $idSala = get_field('idSala');

 if($tieneSala ==1){
  $contSala='<div class="col-8"><iframe width="100%" height="460" src="https://www.youtube.com/embed/'. $idSala .'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
    </div><div class="col-4"><iframe allowfullscreen="" frameborder="0" height="460" src="https://www.youtube.com/live_chat?v='. $idSala .'&amp;is&amp;embed_domain=ecastellanacardio.es" width="100%"></iframe>
    </div>';
 } else {
   $contSala='<div class="col-12">
      <iframe width="100%" height="460" src="https://www.youtube.com/embed/'. $idSala .'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
    </div>';

 }

 ?>


<div class="row">
<?php echo $contSala; ?>
  </div>
<div class="row">
  <div class="col-12">
    @php the_content() @endphp
  </div>
</div>

