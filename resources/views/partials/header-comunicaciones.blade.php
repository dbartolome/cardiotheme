<?php
$imgFondoGenrica = get_field('imagenes_encabezado_comunicaciones', 'option');

?>
<header class="banner" style="background-image: url(<?php echo $imgFondoGenrica['url']; ?>); background-color: #1d2124; text-transform: uppercase; color: #fff;">

  <h1>Comunicaciones</h1>

</header>

