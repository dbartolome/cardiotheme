<?php

  $args = array('post_type'=>'reunion','posts_per_page'=>'-1','orderby'   => 'meta_value_num', 'meta_key'  => 'fechaReunion', 'order' => 'ASC' );
  $loop = new WP_Query($args);
  $i = 1;
  $posicionSlider = 0;
?>




    <div class="glide">
      <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides">
          <?php
         // $number = $loop->post_count;



          ?>
        @while ($loop->have_posts()) @php $loop->the_post() @endphp
   <?php
            $diaReunion = get_field('fechaReunion');
            $diaHoy = date("d-m-Y");

            $datetime1 = new DateTime($diaHoy);
            $datetime2 = new DateTime($diaReunion);
            $interval = $datetime1->diff($datetime2);
            $diferenciaDias = 0;
            $diferenciaDias = $interval->format('%R%a');


            if($diferenciaDias < 0) {
              $posicionSlider = $i -1;
            } else {

            }

            $i++;
            ?>



          @include('partials.content-'.get_post_type())

          @endwhile

        </ul>
      </div>
      <a  class="botonTodas" href="/reuniones/">Ver todas las reuniones</a>
      <div class="glide__arrows" data-glide-el="controls">
          <button class="glide__arrow glide__arrow--left" data-glide-dir="<"> << anterior </button>

          <button class="glide__arrow glide__arrow--right" data-glide-dir=">"> siguiente >> </button>
        </div>

    </div>
<input id="contDiapos" value="<?php echo $posicionSlider; ?>" hidden="true">



