<?php
$imgLogo = get_field('logo-laboratorio');
?>
<article @php post_class() @endphp>
  <div class="row" style="margin-top: -5%;">
    <div class="col-12">
      {!! $slider_colaborador !!}

  </div>
  </div>
<div class="row mt-5 align-items-center">
  <div class="col-12 col-md-3">
      <img src="{!! $imgLogo['url'] !!}" width="100%" alt="<?php the_title() ?>">
  </div>
  <div class="col-12 col-md-9">
  <div class="entry-content">
        @php the_content() @endphp
  </div>
  </div>
</div>
</article>
