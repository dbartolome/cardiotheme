<?php
global $post;
$urlImagen = get_field('portada-comunicacion');
$nombreAutor = get_field('nombre');
$categoriaFilter = '';
$categoriaFilter = get_the_terms( $post->ID, 'categoriacomunicaciones' );
foreach ($categoriaFilter  as $term) {
  $taste_cats = $term->slug;
}
?>
<?php if($taste_cats=="orales"){ ?>
<div class="col-12 col-md-6 col-lg-6 p-3 {!! $taste_cats; !!}" data-categoria="{!! $taste_cats; !!}">
  <a href="{!! the_permalink() !!}"><img src="{!! $urlImagen['url'] !!}" width="100%"></a>
  <div class="nomAutor">{!! $nombreAutor !!}</div>
  <a href="{!! the_permalink() !!}" class="linkComunicacion">{!! get_the_title() !!}</a>
</div>
<?php } else { ?>
<div class="col-12 col-md-6 col-lg-4 p-3 {!! $taste_cats; !!}" data-categoria="{!! $taste_cats; !!}">
    <a href="{!! the_permalink() !!}"><img src="{!! $urlImagen['url'] !!}" width="100%"></a>
    <div class="nomAutor">{!! $nombreAutor !!}</div>
    <a href="{!! the_permalink() !!}" class="linkComunicacion">{!! get_the_title() !!}</a>
</div>
<?php } ?>
