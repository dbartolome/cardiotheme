export default {
  init() {
    // JavaScript to be fired on all pages
    $('.hamburger').click(function () {
      $(this).toggleClass('is-active');
      $('.colNavegacion').toggle('right');
    });

    $('.colNavegacion .nav-link').click(function(){
      $('.colNavegacion').slideToggle();
    })

    $(window).scroll(function() {
      let windowTop = $(document).scrollTop();
      if(windowTop > 5 ) {
        $('.headerMenu').css({
          'position':'fixed',
          'top' : 0,
        })
      } else {
        $('.headerMenu').css({
          'position':'relative',
        })
      }

      if(windowTop > 250 ) {
        $('.btnSubir').fadeIn();
      } else {
        $('.btnSubir').fadeOut();
      }

    });

    $('.btnSubir').click(function(){
      $('body, html').animate({
        scrollTop: '0px',
      }, 500);
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
