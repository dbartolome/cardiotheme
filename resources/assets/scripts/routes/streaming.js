export default {
  init() {
    // JavaScript to be fired on the home page
    function comprobamosFecha() {
      let timeUno = $('#eventoUno').data('fecha');
      let timeDos = $('#eventoDos').data('fecha');
      let timeTres = $('#eventoTres').data('fecha');
      let timeCuatro = $('#eventoCuatro').data('fecha');
      let timeCinco = $('#eventoCinco').data('fecha');
     // console.log('empieza el contador ' +timeUno);
      let d = new Date();
      if(Date.parse(timeUno) < Date.parse(d)) {
        $('#eventoUno').addClass('pasado');
      }
      if(Date.parse(timeDos) < Date.parse(d)) {
        $('#eventoDos').addClass('pasado');
      }
      if(Date.parse(timeTres) < Date.parse(d)) {
        $('#eventoTres').addClass('pasado');
      }
      if(Date.parse(timeCuatro) < Date.parse(d)) {
        $('#eventoCuatro').addClass('pasado');
      }
      if(Date.parse(timeCinco) < Date.parse(d)) {
        $('#eventoCinco').addClass('pasado');
      }
      //var miDate = 'Sat Jun 13 2020 18:36:00 GMT+0200 (hora de verano de Europa central)';
    }
    setInterval( comprobamosFecha , 1000);
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
