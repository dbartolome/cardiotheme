import Glide from '@glidejs/glide'


export default {
  init() {
    // JavaScript to be fired on the home page
    $(document).ready(function() {
      $('a[href^="#"]').click(function() {
        var destino = $(this.hash);
        if (destino.length == 0) {
          destino = $('a[name="' + this.hash.substr(1) + '"]');
        }
        if (destino.length == 0) {
          destino = $('html');
        }
        $('html, body').animate({ scrollTop: destino.offset().top }, 500);
        return false;
      });
    });

    $('.botnDesplegable').on('click',function(){
      let varNumero = $(this).data('toggle');
      $('.collapse').fadeOut();
      $('#collapseExample'+ varNumero).fadeIn();
      $('.botnDesplegable').removeClass('activo');
      $(this).addClass('activo');
    })






    let numPosicion = 0;
    let comienzaPosicion = parseInt($('#contDiapos').val()) ;



    let glide = new Glide('.glide', {
      bound: true,
      type: 'slider',
      focusAt: numPosicion,
      rewind: false,
      gap: 75,
      startAt: comienzaPosicion,
      perView: 1,
      peek: {
        before: 550,
        after: 550,
      },
      breakpoints: {
        600: {
          perView: 1,
          peek: {
            before: 20,
            after: 20,
          },
        },
        990: {
          perView: 1,
          peek: {
            before: 50,
            after: 50,
          },
        },
        1200: {
          gap: 25,
          perView: 1,
          peek: {
            before: 250,
            after: 250,
          },
        },
        1600: {
          gap: 25,
          perView: 1,
          peek: {
            before: 300,
            after: 300,
          },
        },
      },
    });




    glide.mount();




  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
