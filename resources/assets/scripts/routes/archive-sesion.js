/* eslint-disable no-unused-vars */
import 'lightgallery/dist/js/lightgallery';
import 'lightgallery/modules/lg-fullscreen';
import 'lightgallery/modules/lg-hash';
import 'lightgallery/modules/lg-thumbnail'
import 'lightgallery/modules/lg-video'
// import Plyr from 'plyr/dist/plyr';
export default {
  init() {




    $('#video-thumbnails').lightGallery({
      selector: 'a',
      mode: 'lg-fade',
      cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
      thumbnail: 'true',
      thumbWidth: 200,
      thumbheight: 90,
      loadYoutubeThumbnail: true,
      youtubeThumbSize: 'default',
    });

  },
  finalize() {
    // JavaScript to be fired on single post, after the init JS

  },
};
