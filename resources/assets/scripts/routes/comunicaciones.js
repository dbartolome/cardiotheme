export default {
  init() {
    $('.posters').fadeOut();
    $('#verOrales').on('click',function(){
      $('#verPosters').removeClass('activo');
      $('.orales').fadeIn();
      $('.posters').fadeOut();
      $('#verOrales').addClass('activo');
    })
    $('#verPosters').on('click',function(){
      $('#verOrales').removeClass('activo');
      $('.orales').fadeOut();
      $('.posters').fadeIn();
      $('#verPosters').addClass('activo');
    })

  },
};
